package main

import (
	"errors"
	"fmt"
	"git.openprivacy.ca/openprivacy/log"
	"os"
	//"bufio"
	//"cwtch.im/cwtch/storage"
)

func convertTorFile(filename string, password string) error {
	return errors.New("this code doesn't work and can never work :( it's a math thing")

	/*name, _ := diceware.Generate(2)
	sk, err := ioutil.ReadFile("hs_ed25519_secret_key")
	if err != nil {
		return err
	}
	sk = sk[32:]

	pk, err := ioutil.ReadFile("hs_ed25519_public_key")
	if err != nil {
		return err
	}
	pk = pk[32:]

	onion, err := ioutil.ReadFile("hostname")
	if err != nil {
		return err
	}
	onion = onion[:56]

	peer := libpeer.NewCwtchPeer(strings.Join(name, "-"))

	fmt.Printf("%d %d %s\n", len(peer.GetProfile().Ed25519PublicKey), len(peer.GetProfile().Ed25519PrivateKey), peer.GetProfile().Onion)
	peer.GetProfile().Ed25519PrivateKey = sk
	peer.GetProfile().Ed25519PublicKey = pk
	peer.GetProfile().Onion = string(onion)
	fileStore := storage2.NewFileStore(filename, password)
	err = fileStore.save(peer)
	if err != nil {
		return err
	}

	log.Infof("success! loaded %d byte pk and %d byte sk for %s.onion\n", len(pk), len(sk), onion)
	return nil*/
}

/*
func vanity() error {
	for {
		pk, sk, err := ed25519.GenerateKey(rand.Reader)
		if err != nil {
			continue
		}
		onion := utils.GetTorV3Hostname(pk)
		for i := 4; i < len(os.Args); i++ {
			if strings.HasPrefix(onion, os.Args[i]) {
				peer := libpeer.NewCwtchPeer(os.Args[i])
				peer.GetProfile().Ed25519PrivateKey = sk
				peer.GetProfile().Ed25519PublicKey = pk
				peer.GetProfile().Onion = onion
				profileStore, _ := storage2.NewProfileStore(nil, os.Args[3], onion+".cwtch")
				profileStore.Init("")
				// need to signal new onion? impossible
				log.Infof("found %s.onion\n", onion)
			}
		}
	}
}*/

func printHelp() {
	log.Infoln("usage: cwtchutil {help, convert-cwtch-file, convert-tor-file, changepw, vanity}")
}

func main() {
	log.SetLevel(log.LevelInfo)
	if len(os.Args) < 2 {
		printHelp()
		os.Exit(1)
	}

	switch os.Args[1] {
	default:
		printHelp()
	case "help":
		printHelp()
	case "convert-tor-file":
		if len(os.Args) != 4 {
			fmt.Println("example: cwtchutil convert-tor-file /var/lib/tor/hs1 passw0rd")
			os.Exit(1)
		}
		err := convertTorFile(os.Args[2], os.Args[3])
		if err != nil {
			log.Errorln(err)
		}
		/*case "vanity":
		if len(os.Args) < 5 {
			fmt.Println("example: cwtchutil vanity 4 passw0rd erinn openpriv")
			os.Exit(1)
		}

		goroutines, err := strconv.Atoi(os.Args[2])
		if err != nil {
			log.Errorf("first parameter after vanity should be a number\n")
			os.Exit(1)
		}
		log.Infoln("searching. press ctrl+c to stop")
		for i := 0; i < goroutines; i++ {
			go vanity()
		}

		for { // run until ctrl+c
			time.Sleep(time.Hour * 24)
		}*/
		/*case "changepw":
		if len(os.Args) != 3 {
			fmt.Println("example: cwtch changepw ~/.cwtch/profiles/XXX")
			os.Exit(1)
		}

		fmt.Printf("old password: ")
		reader := bufio.NewReader(os.Stdin)
		pw, err := reader.ReadString('\n')
		if err != nil {
			log.Errorln(err)
			os.Exit(1)
		}
		pw = pw[:len(pw)-1]

		profileStore, _ := storage.NewProfileStore(nil, os.Args[2], pw)

		err = profileStore.Read()
		if err != nil {
			log.Errorln(err)
			os.Exit(1)
		}

		fmt.Printf("new password: ")
		newpw1, err := reader.ReadString('\n')
		if err != nil {
			log.Errorln(err)
			os.Exit(1)
		}
		newpw1 = newpw1[:len(newpw1)-1] // fuck go with this linebreak shit ^ea

		fileStore2, _ := storage.NewProfileStore(nil, os.Args[2], newpw1)
		// No way to copy, populate this method
		err = fileStore2.save(peer)
		if err != nil {
			log.Errorln(err)
			os.Exit(1)
		}

		log.Infoln("success!")
		*/
	}
}
