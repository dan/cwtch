package plugins

import (
	"cwtch.im/cwtch/event"
	"git.openprivacy.ca/openprivacy/connectivity"
)

// PluginID is used as an ID for signaling plugin activities
type PluginID int

// These are the plugin IDs for the supplied plugins
const (
	CONNECTIONRETRY PluginID = iota
	NETWORKCHECK
)

// Plugin is the interface for a plugin
type Plugin interface {
	Start()
	Shutdown()
}

// Get is a plugin factory for the requested plugin
func Get(id PluginID, bus event.Manager, acn connectivity.ACN) Plugin {
	switch id {
	case CONNECTIONRETRY:
		return NewConnectionRetry(bus)
	case NETWORKCHECK:
		return NewNetworkCheck(bus, acn)
	}

	return nil
}
