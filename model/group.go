package model

import (
	"crypto/rand"
	"cwtch.im/cwtch/protocol/groups"
	"encoding/json"
	"errors"
	"fmt"
	"git.openprivacy.ca/openprivacy/connectivity/tor"
	"git.openprivacy.ca/openprivacy/log"
	"golang.org/x/crypto/nacl/secretbox"
	"io"
	"sync"
	"time"
)

// CurrentGroupVersion is used to set the version of newly created groups and make sure group structs stored are correct and up to date
const CurrentGroupVersion = 2

// Group defines and encapsulates Cwtch's conception of group chat. Which are sessions
// tied to a server under a given group key. Each group has a set of Messages.
type Group struct {
	GroupID                string
	SignedGroupID          []byte
	GroupKey               [32]byte
	GroupServer            string
	Timeline               Timeline `json:"-"`
	Accepted               bool
	Owner                  string
	IsCompromised          bool
	InitialMessage         []byte
	Attributes             map[string]string
	lock                   sync.Mutex
	LocalID                string
	State                  string `json:"-"`
	UnacknowledgedMessages []Message
	Version                int
}

// NewGroup initializes a new group associated with a given CwtchServer
func NewGroup(server string) (*Group, error) {
	group := new(Group)
	group.Version = CurrentGroupVersion
	group.LocalID = GenerateRandomID()

	if tor.IsValidHostname(server) == false {
		return nil, errors.New("Server is not a valid v3 onion")
	}

	group.GroupServer = server

	var groupID [16]byte
	if _, err := io.ReadFull(rand.Reader, groupID[:]); err != nil {
		log.Errorf("Cannot read from random: %v\n", err)
		return nil, err
	}
	group.GroupID = fmt.Sprintf("%x", groupID)

	var groupKey [32]byte
	if _, err := io.ReadFull(rand.Reader, groupKey[:]); err != nil {
		log.Errorf("Error: Cannot read from random: %v\n", err)
		return nil, err
	}
	copy(group.GroupKey[:], groupKey[:])
	group.Owner = "self"
	group.Attributes = make(map[string]string)
	return group, nil
}

// SignGroup adds a signature to the group.
func (g *Group) SignGroup(signature []byte) {
	g.SignedGroupID = signature
	copy(g.Timeline.SignedGroupID[:], g.SignedGroupID)
}

// Compromised should be called if we detect a a groupkey leak.
func (g *Group) Compromised() {
	g.IsCompromised = true
}

// GetInitialMessage returns the first message of the group, if one was sent with the invite.
func (g *Group) GetInitialMessage() []byte {
	g.lock.Lock()
	defer g.lock.Unlock()
	return g.InitialMessage
}

// Invite generates a invitation that can be sent to a cwtch peer
func (g *Group) Invite(initialMessage []byte) ([]byte, error) {

	if g.SignedGroupID == nil {
		return nil, errors.New("group isn't signed")
	}

	g.InitialMessage = initialMessage[:]

	gci := &groups.GroupInvite{
		GroupName:      g.GroupID,
		SharedKey:      g.GroupKey[:],
		ServerHost:     g.GroupServer,
		SignedGroupID:  g.SignedGroupID[:],
		InitialMessage: initialMessage[:],
	}

	invite, err := json.Marshal(gci)
	return invite, err
}

// AddSentMessage takes a DecryptedGroupMessage and adds it to the Groups Timeline
func (g *Group) AddSentMessage(message *groups.DecryptedGroupMessage, sig []byte) Message {
	g.lock.Lock()
	defer g.lock.Unlock()
	timelineMessage := Message{
		Message:            message.Text,
		Timestamp:          time.Unix(int64(message.Timestamp), 0),
		Received:           time.Unix(0, 0),
		Signature:          sig,
		PeerID:             message.Onion,
		PreviousMessageSig: message.PreviousMessageSig,
		ReceivedByServer:   false,
	}
	g.UnacknowledgedMessages = append(g.UnacknowledgedMessages, timelineMessage)
	return timelineMessage
}

// ErrorSentMessage removes a sent message from the unacknowledged list and sets its error flag if found, otherwise returns false
func (g *Group) ErrorSentMessage(sig []byte, error string) bool {
	g.lock.Lock()
	defer g.lock.Unlock()
	var message *Message

	// Delete the message from the unack'd buffer if it exists
	for i, unAckedMessage := range g.UnacknowledgedMessages {
		if compareSignatures(unAckedMessage.Signature, sig) {
			message = &unAckedMessage
			g.UnacknowledgedMessages = append(g.UnacknowledgedMessages[:i], g.UnacknowledgedMessages[i+1:]...)

			message.Error = error
			g.Timeline.Insert(message)
			return true
		}
	}
	return false
}

// AddMessage takes a DecryptedGroupMessage and adds it to the Groups Timeline
func (g *Group) AddMessage(message *groups.DecryptedGroupMessage, sig []byte) (*Message, bool) {

	g.lock.Lock()
	defer g.lock.Unlock()

	// Delete the message from the unack'd buffer if it exists
	for i, unAckedMessage := range g.UnacknowledgedMessages {
		if compareSignatures(unAckedMessage.Signature, sig) {
			g.UnacknowledgedMessages = append(g.UnacknowledgedMessages[:i], g.UnacknowledgedMessages[i+1:]...)
			break
		}
	}

	timelineMessage := &Message{
		Message:            message.Text,
		Timestamp:          time.Unix(int64(message.Timestamp), 0),
		Received:           time.Now(),
		Signature:          sig,
		PeerID:             message.Onion,
		PreviousMessageSig: message.PreviousMessageSig,
		ReceivedByServer:   true,
		Error:              "",
	}
	seen := g.Timeline.Insert(timelineMessage)

	return timelineMessage, seen
}

// GetTimeline provides a safe copy of the timeline
func (g *Group) GetTimeline() (timeline []Message) {
	g.lock.Lock()
	defer g.lock.Unlock()
	return append(g.Timeline.GetMessages(), g.UnacknowledgedMessages...)
}

//EncryptMessage takes a message and encrypts the message under the group key.
func (g *Group) EncryptMessage(message *groups.DecryptedGroupMessage) ([]byte, error) {
	var nonce [24]byte
	if _, err := io.ReadFull(rand.Reader, nonce[:]); err != nil {
		log.Errorf("Cannot read from random: %v\n", err)
		return nil, err
	}
	wire, err := json.Marshal(message)
	if err != nil {
		return nil, err
	}
	encrypted := secretbox.Seal(nonce[:], []byte(wire), &nonce, &g.GroupKey)
	return encrypted, nil
}

// DecryptMessage takes a ciphertext and returns true and the decrypted message if the
// cipher text can be successfully decrypted,else false.
func (g *Group) DecryptMessage(ciphertext []byte) (bool, *groups.DecryptedGroupMessage) {
	if len(ciphertext) > 24 {
		var decryptNonce [24]byte
		copy(decryptNonce[:], ciphertext[:24])
		decrypted, ok := secretbox.Open(nil, ciphertext[24:], &decryptNonce, &g.GroupKey)
		if ok {
			dm := &groups.DecryptedGroupMessage{}
			err := json.Unmarshal(decrypted, dm)
			if err == nil {
				return true, dm
			}
		}
	}
	return false, nil
}

// SetAttribute allows applications to store arbitrary configuration info at the group level.
func (g *Group) SetAttribute(name string, value string) {
	g.lock.Lock()
	defer g.lock.Unlock()
	g.Attributes[name] = value
}

// GetAttribute returns the value of a value set with SetAttribute. If no such value has been set exists is set to false.
func (g *Group) GetAttribute(name string) (value string, exists bool) {
	g.lock.Lock()
	defer g.lock.Unlock()
	value, exists = g.Attributes[name]
	return
}
