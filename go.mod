module cwtch.im/cwtch

go 1.14

require (
	cwtch.im/tapir v0.2.1
	git.openprivacy.ca/openprivacy/connectivity v1.3.0
	git.openprivacy.ca/openprivacy/log v1.0.1
	github.com/gtank/ristretto255 v0.1.2
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/struCoder/pidusage v0.1.3
	golang.org/x/crypto v0.0.0-20201012173705-84dcc777aaee
	golang.org/x/net v0.0.0-20201010224723-4f7140c49acb
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
