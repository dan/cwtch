// Known race issue with event bus channel closure

package v0

import (
	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/model"
	"log"
	"os"
	"testing"
	"time"
)

const testProfileName = "Alice"
const testKey = "key"
const testVal = "value"
const testInitialMessage = "howdy"
const testMessage = "Hello from storage"

// NewProfile creates a new profile for use in the profile store.
func NewProfile(name string) *model.Profile {
	profile := model.GenerateNewProfile(name)
	return profile
}

func TestProfileStoreWriteRead(t *testing.T) {
	log.Println("profile store test!")
	os.RemoveAll(testingDir)
	eventBus := event.NewEventManager()
	profile := NewProfile(testProfileName)
	ps1 := NewProfileWriterStore(eventBus, testingDir, password, profile)

	profile.SetAttribute(testKey, testVal)

	groupid, invite, err := profile.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	if err != nil {
		t.Errorf("Creating group: %v\n", err)
	}
	if err != nil {
		t.Errorf("Creating group invite: %v\n", err)
	}

	ps1.AddGroup(invite, profile.Onion)

	ps1.AddGroupMessage(groupid, time.Now().Format(time.RFC3339Nano), time.Now().Format(time.RFC3339Nano), ps1.getProfileCopy(true).Onion, testMessage)

	ps1.Shutdown()

	ps2 := NewProfileWriterStore(eventBus, testingDir, password, nil)
	err = ps2.Load()
	if err != nil {
		t.Errorf("Error createing ProfileStoreV0: %v\n", err)
	}

	profile = ps2.getProfileCopy(true)
	if profile.Name != testProfileName {
		t.Errorf("Profile name from loaded profile incorrect. Expected: '%v' Actual: '%v'\n", testProfileName, profile.Name)
	}

	v, _ := profile.GetAttribute(testKey)
	if v != testVal {
		t.Errorf("Profile attribute '%v' incorrect. Expected: '%v' Actual: '%v'\n", testKey, testVal, v)
	}

	group2 := ps2.getProfileCopy(true).Groups[groupid]
	if group2 == nil {
		t.Errorf("Group not loaded\n")
	}

}
