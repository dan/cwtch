package main

import (
	app2 "cwtch.im/cwtch/app"
	"cwtch.im/cwtch/app/utils"
	"git.openprivacy.ca/openprivacy/connectivity/tor"
	"git.openprivacy.ca/openprivacy/log"
	"os"
	"path"
	"time"
)

func main() {

	// System Boilerplate, We need Tor Up and Running
	log.AddEverythingFromPattern("peer/bob")
	log.SetLevel(log.LevelDebug)
	acn, err := tor.NewTorACN(path.Join(".", ".cwtch"), "")
	if err != nil {
		log.Errorf("\nError connecting to Tor: %v\n", err)
		os.Exit(1)
	}

	app := app2.NewApp(acn, ".")
	app.CreatePeer("bob", "be gay, do crimes")
	bob := utils.WaitGetPeer(app, "bob")

	// Add Alice's Onion Here (It changes run to run)
	bob.PeerWithOnion("upiztu7myymjf2dn4x4czhagp7axlnqjvf5zwfegbhtpkqb6v3vgu5yd")

	// Send the Message...
	log.Infof("Waiting for Bob to Connect to Alice...")
	bob.SendMessageToPeer("upiztu7myymjf2dn4x4czhagp7axlnqjvf5zwfegbhtpkqb6v3vgu5yd", "Hello Alice!!!")

	// Wait a while...
	// Everything is run in  a goroutine so the main thread has to stay active
	time.Sleep(time.Second * 100)

}
