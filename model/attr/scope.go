package attr

import (
	"strings"
)

/*
Scope model for peer attributes and requests

A local peer "Alice" has a PublicScope that is queryable by getVal requests.
By default, for now, other scopes are private, of which we here define SettingsScope

Alice's peer structs of remote peers such as "Bob" keep the queried
PublicScope values in the PeerScope, which can be overridden by the same named
values stored in the LocalScope.

*/

// scopes for attributes
const (
	// on a peer, local and peer supplied data
	LocalScope = "local"
	PeerScope  = "peer"

	// on a local profile, public data and private settings
	PublicScope   = "public"
	SettingsScope = "settings"
)

// Separator for scope and the rest of path
const Separator = "."

// GetPublicScope takes a path and attaches the pubic scope to it
func GetPublicScope(path string) string {
	return PublicScope + Separator + path
}

// GetSettingsScope takes a path and attaches the settings scope to it
func GetSettingsScope(path string) string {
	return SettingsScope + Separator + path
}

// GetLocalScope takes a path and attaches the local scope to it
func GetLocalScope(path string) string {
	return LocalScope + Separator + path
}

// GetPeerScope takes a path and attaches the peer scope to it
func GetPeerScope(path string) string {
	return PeerScope + Separator + path
}

// GetScopePath take a full path and returns the scope and the scope-less path
func GetScopePath(fullPath string) (string, string) {
	parts := strings.SplitN(fullPath, Separator, 1)
	if len(parts) != 2 {
		return "", ""
	}
	return parts[0], parts[1]
}
