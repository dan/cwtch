// Known race issue with event bus channel closure

package v1

import (
	"cwtch.im/cwtch/event"
	"fmt"
	"log"
	"os"
	"testing"
	"time"
)

const testProfileName = "Alice"
const testKey = "key"
const testVal = "value"
const testInitialMessage = "howdy"
const testMessage = "Hello from storage"

func TestProfileStoreWriteRead(t *testing.T) {
	log.Println("profile store test!")
	os.RemoveAll(testingDir)
	eventBus := event.NewEventManager()
	profile := NewProfile(testProfileName)
	ps1 := CreateProfileWriterStore(eventBus, testingDir, password, profile)

	eventBus.Publish(event.NewEvent(event.SetAttribute, map[event.Field]string{event.Key: testKey, event.Data: testVal}))
	time.Sleep(1 * time.Second)

	groupid, invite, err := profile.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	if err != nil {
		t.Errorf("Creating group: %v\n", err)
	}
	if err != nil {
		t.Errorf("Creating group invite: %v\n", err)
	}

	eventBus.Publish(event.NewEvent(event.NewGroupInvite, map[event.Field]string{event.TimestampReceived: time.Now().Format(time.RFC3339Nano), event.RemotePeer: ps1.GetProfileCopy(true).Onion, event.GroupInvite: string(invite)}))
	time.Sleep(1 * time.Second)

	eventBus.Publish(event.NewEvent(event.NewMessageFromGroup, map[event.Field]string{
		event.GroupID:           groupid,
		event.TimestampSent:     time.Now().Format(time.RFC3339Nano),
		event.TimestampReceived: time.Now().Format(time.RFC3339Nano),
		event.RemotePeer:        ps1.GetProfileCopy(true).Onion,
		event.Data:              testMessage,
	}))
	time.Sleep(1 * time.Second)

	ps1.Shutdown()

	ps2, err := LoadProfileWriterStore(eventBus, testingDir, password)
	if err != nil {
		t.Errorf("Error createing ProfileStoreV1: %v\n", err)
	}

	profile = ps2.GetProfileCopy(true)
	if profile.Name != testProfileName {
		t.Errorf("Profile name from loaded profile incorrect. Expected: '%v' Actual: '%v'\n", testProfileName, profile.Name)
	}

	v, _ := profile.GetAttribute(testKey)
	if v != testVal {
		t.Errorf("Profile attribute '%v' inccorect. Expected: '%v' Actual: '%v'\n", testKey, testVal, v)
	}

	group2 := ps2.GetProfileCopy(true).Groups[groupid]
	if group2 == nil {
		t.Errorf("Group not loaded\n")
	}

}

func TestProfileStoreChangePassword(t *testing.T) {
	os.RemoveAll(testingDir)
	eventBus := event.NewEventManager()

	queue := event.NewQueue()
	eventBus.Subscribe(event.ChangePasswordSuccess, queue)

	profile := NewProfile(testProfileName)
	ps1 := CreateProfileWriterStore(eventBus, testingDir, password, profile)

	groupid, invite, err := profile.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	if err != nil {
		t.Errorf("Creating group: %v\n", err)
	}
	if err != nil {
		t.Errorf("Creating group invite: %v\n", err)
	}

	eventBus.Publish(event.NewEvent(event.NewGroupInvite, map[event.Field]string{event.TimestampReceived: time.Now().Format(time.RFC3339Nano), event.RemotePeer: ps1.GetProfileCopy(true).Onion, event.GroupInvite: string(invite)}))
	time.Sleep(1 * time.Second)

	fmt.Println("Sending 200 messages...")

	for i := 0; i < 200; i++ {
		eventBus.Publish(event.NewEvent(event.NewMessageFromGroup, map[event.Field]string{
			event.GroupID:           groupid,
			event.TimestampSent:     time.Now().Format(time.RFC3339Nano),
			event.TimestampReceived: time.Now().Format(time.RFC3339Nano),
			event.RemotePeer:        profile.Onion,
			event.Data:              testMessage,
		}))
	}

	newPass := "qwerty123"

	fmt.Println("Sending Change Passwords event...")
	eventBus.Publish(event.NewEventList(event.ChangePassword, event.Password, password, event.NewPassword, newPass))

	ev := queue.Next()
	if ev.EventType != event.ChangePasswordSuccess {
		t.Errorf("Unexpected event response detected %v\n", ev.EventType)
		return
	}

	fmt.Println("Sending 10 more messages...")
	for i := 0; i < 10; i++ {
		eventBus.Publish(event.NewEvent(event.NewMessageFromGroup, map[event.Field]string{
			event.GroupID:           groupid,
			event.TimestampSent:     time.Now().Format(time.RFC3339Nano),
			event.TimestampReceived: time.Now().Format(time.RFC3339Nano),
			event.RemotePeer:        profile.Onion,
			event.Data:              testMessage,
		}))
	}
	time.Sleep(3 * time.Second)

	fmt.Println("Shutdown profile store...")
	ps1.Shutdown()

	fmt.Println("New Profile store...")
	ps2, err := LoadProfileWriterStore(eventBus, testingDir, newPass)
	if err != nil {
		t.Errorf("Error createing new ProfileStoreV1 with new password: %v\n", err)
		return
	}

	profile2 := ps2.GetProfileCopy(true)

	if profile2.Groups[groupid] == nil {
		t.Errorf("Failed to load group %v\n", groupid)
		return
	}

	if len(profile2.Groups[groupid].Timeline.Messages) != 210 {
		t.Errorf("Failed to load group's 210 messages, instead got %v\n", len(profile2.Groups[groupid].Timeline.Messages))
	}
}
