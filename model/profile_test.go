package model

import (
	"testing"
)

func TestProfileIdentity(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")

	alice.AddContact(sarah.Onion, &sarah.PublicProfile)
	if alice.Contacts[sarah.Onion].Name != "Sarah" {
		t.Errorf("alice should have added sarah as a contact %v", alice.Contacts)
	}

	if len(alice.GetContacts()) != 1 {
		t.Errorf("alice should be only contact: %v", alice.GetContacts())
	}

	alice.SetAttribute("test", "hello world")
	value, _ := alice.GetAttribute("test")
	if value != "hello world" {
		t.Errorf("value from custom attribute should have been 'hello world', instead was: %v", value)
	}

	t.Logf("%v", alice)
}

func TestTrustPeer(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)
	alice.SetContactAuthorization(sarah.Onion, AuthApproved)
	if alice.GetContactAuthorization(sarah.Onion) != AuthApproved {
		t.Errorf("peer should be approved")
	}
}

func TestBlockPeer(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)
	alice.SetContactAuthorization(sarah.Onion, AuthBlocked)
	if alice.GetContactAuthorization(sarah.Onion) != AuthBlocked {
		t.Errorf("peer should be blocked")
	}

	if alice.SetContactAuthorization("", AuthUnknown) == nil {
		t.Errorf("Seting Auth level of a non existent peer should error")
	}
}

func TestAcceptNonExistentGroup(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	sarah.AcceptInvite("doesnotexist")
}

func TestRejectGroupInvite(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)

	gid, invite, _ := alice.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	sarah.ProcessInvite(string(invite), alice.Onion)
	group := alice.GetGroup(gid)
	if len(sarah.Groups) == 1 {
		if sarah.GetGroup(group.GroupID).Accepted {
			t.Errorf("Group should not be accepted")
		}
		sarah.RejectInvite(group.GroupID)
		if len(sarah.Groups) != 0 {
			t.Errorf("Group %v should have been deleted", group.GroupID)
		}
		return
	}
	t.Errorf("Group should exist in map")
}

func TestProfileGroup(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)

	gid, invite, _ := alice.StartGroupWithMessage("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd", []byte("Hello World"))
	sarah.ProcessInvite(string(invite), alice.Onion)
	if len(sarah.GetGroups()) != 1 {
		t.Errorf("sarah should only be in 1 group instead: %v", sarah.GetGroups())
	}

	group := alice.GetGroup(gid)
	sarah.AcceptInvite(group.GroupID)
	c, s1, _ := sarah.EncryptMessageToGroup("Hello World", group.GroupID)
	alice.AttemptDecryption(c, s1)

	gid2, invite2, _ := alice.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	sarah.ProcessInvite(string(invite2), alice.Onion)
	group2 := alice.GetGroup(gid2)
	c2, s2, _ := sarah.EncryptMessageToGroup("Hello World", group2.GroupID)
	alice.AttemptDecryption(c2, s2)

	sarahGroup := sarah.GetGroup(group.GroupID)
	im := sarahGroup.GetInitialMessage()
	if string(im) != "Hello World" {
		t.Errorf("Initial Message was not stored properly: %v", im)
	}

	_, _, err := sarah.EncryptMessageToGroup(string(make([]byte, MaxGroupMessageLength*2)), group2.GroupID)
	if err == nil {
		t.Errorf("Overly long message should have returned an error")
	}

	bob := GenerateNewProfile("bob")
	bob.AddContact(alice.Onion, &alice.PublicProfile)
	bob.ProcessInvite(string(invite2), alice.Onion)
	c3, s3, err := bob.EncryptMessageToGroup("Bobs Message", group2.GroupID)
	if err == nil {
		ok, _, message, _ := alice.AttemptDecryption(c3, s3)
		if !ok {
			t.Errorf("Bobs message to the group should be decrypted %v %v", message, ok)
		}

		eve := GenerateNewProfile("eve")
		ok, _, _, _ = eve.AttemptDecryption(c3, s3)
		if ok {
			t.Errorf("Eves hould not be able to decrypt Messages!")
		}
	} else {
		t.Errorf("Bob failed to encrypt a message to the group")
	}
}
